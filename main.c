#include <stdio.h>
#include <curl/curl.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define CRYPTOCOMPARE 1
#define COINBASE 2
#define BINANCE 3

#define CRYPTOCOMPARE_SINGLE_PRICE "https://min-api.cryptocompare.com/data/price?fsym=%s&tsyms=USD"
#define CRYPTOCOMPARE_MULTI_PRICE "https://min-api.cryptocompare.com/data/pricemulti?fsyms=%s&tsyms=USD"
#define CRYPTOCOMPARE_PRICE_HISTORICAL "https://min-api.cryptocompare.com/data/histoday?fsym=%s&tsym=%s&limit=%d&toTs=%d"
// %s in the following define is a Coinbase-style currency pairing (i.e. "ETH-USD")
// The date is specified as YYYY-MM-DD UTC
#define COINBASE_PRICE_ENDPOINT "https://api.coinbase.com/v2/prices/%s/spot"
// %s in the following define is a Binance-style currency pairing (i.e. ETHBTC)
#define BINANCE_PRICE_ENDPOINT "https://api.binance.com/api/v1/ticker/price?symbol=%s"

#define COLOR_RED "\033[31m"
#define COLOR_GREEN "\033[32m"
#define COLOR_RESET "\033[37;0m"

struct ResponseData {
				char* htmlData;
};

struct CLIArgs{
				short currencyCount;
				char** currencies;
				char exchange;
};

size_t handleRemoteResponse(char *ptr, size_t size, size_t nmemb, void* userdata);
char* getJSONLevel(char* jsonString, char* key);
char* cleanupJSONTier(char* tier);
char* retrieveCurrentPrice(char* data, char* ticker);
struct CLIArgs* doArgs(int argc, char** argv);
float* getHistoricalPrices(CURL* curl, CURLcode* res, char exchange, char** tickers, int tickerCount, time_t desiredTime);
short countAllStringVars(char* string);
char* cleanupJSONEntry(char* tier);
float* getSpotPrice(CURL* curl, CURLcode* res, char exchange, char** tickers, int tickerCount);

int main(int argc, char** argv){
				CURL* curl;
				CURLcode res;

				curl = curl_easy_init();

				struct CLIArgs* cliArgs = doArgs(argc, argv);
				if(cliArgs->currencyCount == 0){
								// Insert some default currencies to be processed.
								cliArgs->currencies = (char**)malloc(sizeof(char*)*4);
								cliArgs->currencies[0] = "BTC";
								cliArgs->currencies[1] = "LTC";
								cliArgs->currencies[2] = "ETH";
								cliArgs->currencies[3] = "XMR";
								cliArgs->currencyCount = 4;
				}
				float* spotPrices = getSpotPrice(curl, &res, cliArgs->exchange, cliArgs->currencies, cliArgs->currencyCount);
				float* oneHourPrices = getHistoricalPrices(curl, &res, cliArgs->exchange, cliArgs->currencies, cliArgs->currencyCount, time(NULL));
				// We now have all of the spot prices at the time the command was executed.
				for(short c=0;c!=cliArgs->currencyCount;c++){
								printf("%s:", cliArgs->currencies[c]);
								printf("%10.4f", spotPrices[c]);
								puts("");
				}
				return 0;
}

size_t handleRemoteResponse(char *ptr, size_t size, size_t nmemb, void* userdata){
				struct ResponseData* thisResponse = (struct ResponseData*)userdata;
				thisResponse->htmlData = (char*)malloc(size*nmemb) + sizeof(char);
				memcpy(thisResponse->htmlData, ptr, size*nmemb);
				thisResponse->htmlData[size*nmemb] = 0;
				return size*nmemb;
}

// The following is a barely function pseudo-tokenizer monstrosity that is here
// for the sole purpose of avoiding linking a full JSON parsing library.
// It works just well enough to get the job done.  I don't expect any awards.
char* getJSONLevel(char* jsonString, char* key){
				// Scan through the provided JSON string until the proper key is found.
				char* keyLocation = strstr(jsonString, key);
				// We now have the key location, locate the end of it and return this
				// value.
				char* returnVal = strstr(keyLocation,":\0");
				return returnVal;
}

// Truncate at the first occurence of '}'
char* cleanupJSONTier(char* tier){
				tier += 1;
				char* breakPos = strstr(tier, "}");
				char* returnString = (char*)malloc(sizeof(char)*(breakPos-tier));
				memcpy(returnString, tier, (int)(breakPos-tier));
				returnString[breakPos-tier] = 0;
				return returnString;
};

// TODO: Optimize this function.
char* cleanupJSONEntry(char* tier){
				char* breakPos = strstr(tier, ",");
				char* returnString = (char*)malloc(sizeof(char)*(breakPos-tier)+1);
				memcpy(returnString,tier,breakPos-tier);
				returnString[(breakPos-tier)] = 0;
				returnString = &returnString[1];
				return returnString;
}

char* retrieveCurrentPrice(char* data, char* ticker){
				char* thisStart = getJSONLevel(data, ticker);				
				char* usd = getJSONLevel(thisStart, "USD");
				usd = cleanupJSONTier(usd);
				return usd;
}

struct CLIArgs* doArgs(int argc, char** argv){
				// Parse the flags given to the software on the commandline.
				struct CLIArgs* returnArgs = (struct CLIArgs*)malloc(sizeof(struct CLIArgs));
				returnArgs->exchange = CRYPTOCOMPARE;
				returnArgs->currencyCount= 0;

				if(argc == 1){ // No arguments were specified.
								return returnArgs;
				}
				// Arguments have been specified.  Process them.
				for(int c=1;c!=argc;c++){
								char* needle = (char*)malloc(sizeof(char)*2);
								needle = "-f";
								if(strstr(argv[c], needle)){
												// The currency flag has been specified.  Parse out the
												// list and assign it to the struct.
												char* listStart = argv[c+1];
												// We should now be at the list start.
												// Determine the number of currencies in the list based
												// on the number of commas within the string.
												// Generate a 2-dimensional array containing all of the
												// specified currencies.
												short numCurrencies = 0;
												for(short x=0;x!=strlen(listStart);x++){
																if(listStart[x] == ','){
																				numCurrencies++;
																}
												}
												numCurrencies++;
												returnArgs->currencyCount = numCurrencies;
												// We now have the total number of currencies, allocate
												// space for them.
												returnArgs->currencies = (char**)malloc(sizeof(char*)*numCurrencies);
												// The loop for each character in the list.
												short rawTickerLen = 0;
												short currentCount = 0;
												char* tickerPtr = listStart;
												for(short lc=0;lc!=strlen(listStart)+1;lc++){
																// Keep counting until either EOL or the first comma.
																if(listStart[lc] != ',' && listStart[lc] != 0){
																				rawTickerLen++;
																}else{
																				// Determine the number of spaces.
																				short numNonSpaces = 0;
																				for(short v=0;v!=rawTickerLen;v++){
																								if(tickerPtr[v] != ' '){
																												numNonSpaces++;
																								}
																				}
																				char* thisString = (char*)malloc(sizeof(char)*numNonSpaces);
																				short currentPos = 0;
																				for(short v=0;v!=rawTickerLen;v++){
																								if(tickerPtr[v] != ' '){
																												thisString[currentPos] = tickerPtr[v];
																												currentPos++;
																								}
																				}
																				// Convert the letters to uppercase for
																				// particularly picky APIs
																				for(short v=0;v!=numNonSpaces;v++){
																								thisString[v] = toupper(thisString[v]);
																				}	
																				thisString[numNonSpaces] = '\0';
																				tickerPtr = &listStart[lc]+1;
																				rawTickerLen = 0;
																				returnArgs->currencies[currentCount] = thisString;
																				currentCount++;
																}
												}
								}
								// Now check if a specific exchange was specified.
								if(strstr(argv[c], "-e")){ // The exchange flag has been given.
												if(strstr(argv[c+1], "cryptocompare"))
																{returnArgs->exchange = CRYPTOCOMPARE;}	
												if(strstr(argv[c+1], "coinbase"))
																{returnArgs->exchange = COINBASE;}
												if(strstr(argv[c+1], "binance"))
																				{returnArgs->exchange = BINANCE;}
								}
				}
				return returnArgs;
}

// TODO: Optimize this
float* getHistoricalPrices(CURL* curl, CURLcode* res, char exchange, char** tickers, int tickerCount, time_t desiredTime){
				int thisRequestStringLength = 0;
				char* thisRequestString = NULL;
				switch(exchange){
								case CRYPTOCOMPARE:
												thisRequestStringLength = strlen(CRYPTOCOMPARE_PRICE_HISTORICAL); 
												// Replace all instances of %s and %d as they will not be
												// factored in to the final output.
												char* currentStringLocation = CRYPTOCOMPARE_PRICE_HISTORICAL;
												char* stringLocation = currentStringLocation;
												char* decimalLocation = currentStringLocation;
												// TODO: Optimize the memory usage of this algorithm.
												for(char c=0;c!=tickerCount;c++){
																thisRequestStringLength = strlen(CRYPTOCOMPARE_PRICE_HISTORICAL) + strlen(tickers[c]);
																thisRequestString = (char*)malloc(sizeof(char)*thisRequestStringLength);
																sprintf(thisRequestString, CRYPTOCOMPARE_PRICE_HISTORICAL, tickers[c], "USD", 1, time(NULL));
																puts(thisRequestString);
												}
												break;
				}
				return 0;
}

short countAllStringVars(char* string){
				short numFoundVariables = 0;
				// Loop through all characters in the string.
				for(short c=0;c!=strlen(string);c++){
								if(string[c] == '%'){ // Potential match.
												switch(string[c+1]){
																case 'd':
																case 's':
																				numFoundVariables++;
																				break;
												}	
								}
				}
				return numFoundVariables;
}

float* getSpotPrice(CURL* curl, CURLcode* res, char exchange, char** tickers, int tickerCount){
				struct ResponseData responseData;
				float* returnVals = (float*)malloc(sizeof(float)*tickerCount);
				int stringLen = 0;
				int listPosition = 0;

			  // Generate the list of currencies as a character array.	
				int currencyListLen = 0;
				for(short c=0;c!=tickerCount;c++){
								currencyListLen += strlen(tickers[c])+1;
				}
				char* currencyList = (char*)malloc(currencyListLen);
				// Generate the URL to retrieve from based on the exchange that was specified.
				// Manually copy the first currency to initialize the memory correctly.
				memcpy(currencyList, tickers[0], strlen(tickers[0]));
				currencyList[strlen(tickers[0])] = ',';
				currencyList[strlen(tickers[0])+1] = '\0';
				for(short c=1;c!=tickerCount;c++){
								currencyList = strcat(currencyList, tickers[c]);
								currencyList = strcat(currencyList, ",");
				}
				char* requestURL = NULL;
				int requestURLLength = 0;
				curl_easy_setopt(curl, CURLOPT_WRITEDATA, &responseData);
				curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, handleRemoteResponse);
				switch(exchange){
								// TODO: Tighten the memory requirements
								// TODO: Check for error responses from remote host.
								case CRYPTOCOMPARE:
												requestURLLength = strlen(CRYPTOCOMPARE_MULTI_PRICE) + strlen(currencyList);
												requestURL = (char*)malloc(sizeof(char) * requestURLLength);
												sprintf(requestURL, CRYPTOCOMPARE_MULTI_PRICE, currencyList);
												curl_easy_setopt(curl, CURLOPT_URL, requestURL);
												curl_easy_perform(curl);
												char* currentLocation = responseData.htmlData;
												for(short c=0;c!=tickerCount;c++){
																currentLocation = getJSONLevel(currentLocation, tickers[c]);
																// Now get the price in the given currency.
																// TODO: Support more than just USD.
																currentLocation = getJSONLevel(currentLocation, "USD");
																returnVals[c] = atof(cleanupJSONTier(currentLocation));
												}
												free(requestURL);
												break;
								case COINBASE:
												// Coinbase's API doesn't allow for bulk transfers,
												// meaning that one HTTP request is required per
												// currency.  Inefficient.
												for(char c=0;c!=tickerCount;c++){
																// Generate the URL for this specific currency pair.
																int requestURLLen = strlen(COINBASE_PRICE_ENDPOINT) + strlen(tickers[c]) + strlen("-USD");
																char* requestURL = (char*)malloc(sizeof(char)*requestURLLen);
																int currencyPairLen = strlen(tickers[c]) + strlen("-USD");
																char* currencyPair = (char*)malloc(sizeof(char)*currencyPairLen);
																sprintf(currencyPair, "%s-USD", tickers[c]);
																sprintf(requestURL, COINBASE_PRICE_ENDPOINT, currencyPair);
																// We've generated the endpoint URL for this currency.
																curl_easy_setopt(curl, CURLOPT_URL, requestURL);
																curl_easy_perform(curl);
																// Extract the price.
																char* currentLocation = getJSONLevel(responseData.htmlData, "amount");
																currentLocation = cleanupJSONTier(currentLocation);
																// Remove the surrounding quotes
																currentLocation += 1;
																currentLocation[strlen(currentLocation)-1] = '\0';
																returnVals[c] = atof(currentLocation);
																free(requestURL);
																free(currencyPair);
												}
												break;
								case BINANCE:
												// Binance does not trade directly in USD, so USDT
												// will be used instead.  It also does not support
												// bulk requests, so like Coinbase it is inefficient.
												for(char c=0;c!=tickerCount;c++){
																int requestURLLen = strlen(BINANCE_PRICE_ENDPOINT) + strlen(tickers[c]);
																char* requestURL = (char*)malloc(sizeof(char)*requestURLLen);
																int currencyPairLen = strlen(tickers[c]) + strlen("USDT\0");
																char* currencyPair = (char*)malloc(sizeof(char)*currencyPairLen);
																sprintf(currencyPair, "%s%s", tickers[c], "USDT\0");
																sprintf(requestURL, BINANCE_PRICE_ENDPOINT, currencyPair);
																curl_easy_setopt(curl, CURLOPT_URL, requestURL);
																curl_easy_perform(curl);
																char* currentLocation = getJSONLevel(responseData.htmlData, "price");
																currentLocation = cleanupJSONTier(currentLocation);
																// Remove the quotations
																currentLocation += 1;
																currentLocation[strlen(currentLocation)-1] = '\0';
																returnVals[c] = atof(currentLocation);
												}
												break;
				}
				return returnVals;
}
